from .envtype import EnvType
from . import cmd

class Python(EnvType):
    specs: list[str]

    def installScript(self) -> cmd.Script:
        slist = " ".join(self.specs)
        return cmd.runonce("[ -f $prefix/pyvenv.cfg ] || python3 -m venv $prefix",
                           "VIRTUAL_ENV = $prefix",
                           f"$prefix/bin/pip install {slist}")
    def loadScript(self) -> cmd.Script:
        return cmd.run("VIRTUAL_ENV = $prefix")
