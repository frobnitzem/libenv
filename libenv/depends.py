""" Recursively download and interpret (as inlined)
    environment specs from URL-s.

    We assume that all dependencies can be installed
    in parallel.  Each install gets run in a shell with
    the "load" step from all previous steps.

    After installing, the "load" steps from this block
    done in serial.

    TODO: re-implement cmd.Script class to describe this
    idea of parallel steps which take load-scripts as histories
    and create a "load then run make" script as their install
    process.

    This involves clever naming that will reveal common
    package dependencies (e.g. one subdir per dependency).
"""
from typing import Any
from tempfile import NamedTemporaryFile

import yaml

#from aurl import URL
from aurl import arun
from aurl.fetch import download_url

from .envtype import EnvType
from . import cmd
from .spec import read_data, load_env

# TODO: force URL to refer to a git repo in the form,
# git+https://gitlab.com/frobnitzem/libenv.git
#
# then construct the download from
# git+https://gitlab.com/frobnitzem/libenv.git@<tag>:env.yaml
#
# i.e. repo = https://gitlab.com/frobnitzem/libenv.git
#      rev-parse = <tag>:env.yaml
async def fetch_env(url : str) -> Any:
    with NamedTemporaryFile(mode="r", encoding="utf-8") as f:
        await download_url(f.name, url)
        attr = yaml.safe_load(f)
    return load_env(attr)

class Depends(EnvType):
    urls : list[str]

    def installScript(self) -> cmd.Script:
        script = cmd.Script()
        for url in self.urls:
            specs = arun(fetch_env(url))
            for spec in specs:
                script.extend( spec.installScript() )
        return script
    def loadScript(self) -> cmd.Script:
        script = cmd.Script()
        for url in self.urls:
            specs = arun(fetch_env(url))
            for spec in specs:
                script.extend( spec.loadScript() )
        return script
