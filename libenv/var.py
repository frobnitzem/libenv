from .envtype import EnvType
from . import cmd

class Var(EnvType):
    vars: dict[str,str]

    #def testScript(self) -> cmd.Script:
    #    """ TestScripts may return 3 different values,
    #        0 = success
    #        1 = not present (install needed)
    #        2 = present, but misconfigured/incompatible
    #    """
    #    return cmd.run("@ { exit 1 }")
    def installScript(self) -> cmd.Script:
        return cmd.Script()
    def loadScript(self) -> cmd.Script:
        return cmd.run(*[f"{k} = {cmd.quote(v)}" for k,v in self.vars.items()])
