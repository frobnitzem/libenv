from .envtype import EnvType
from . import cmd

class Module(EnvType):
    specs: list[str]

    def installScript(self) -> cmd.Script:
        # load the modules to ensure they are available to the
        # loadScript
        return cmd.run(*[f"module load {cmd.quote(s)}" for s in self.specs])
    def loadScript(self) -> cmd.Script:
        return cmd.run(*[f"module load {cmd.quote(s)}" for s in self.specs])
