from typing import Optional, List, Any
from pathlib import Path
from typing_extensions import Annotated
import logging
_logger = logging.getLogger(__name__)

import typer
from aurl import arun

from .config import load_config, Config
from .spec import load_env, read_data
from .var import Var
from . import cmd

def setup_logging(v, vv):
    if vv:
        logging.basicConfig(level=logging.DEBUG)
    elif v:
        logging.basicConfig(level=logging.INFO)

app = typer.Typer()

V1 = Annotated[bool, typer.Option("-v", help="show info-level logs")]
V2 = Annotated[bool, typer.Option("-vv", help="show debug-level logs")]
CfgArg = Annotated[Optional[Path], typer.Option("--config",
                   envvar="LIBENV_CONFIG",
                   help="Config file path [default ~/.config/libenv.json].")]

def prelude(config : Config, name : str) -> cmd.Script:
    prefix = config.data_dir / name
    scr = Var(vars={"prefix": str(prefix)}).loadScript() \
            .extend( cmd.runonce( f"PATH={prefix}/bin:$PATH",
                     f"CMAKE_PREFIX_PATH={prefix}:$CMAKE_PREFIX_PATH",
                     f"MANPATH={prefix}/share/man:$MANPATH",
                     f"PKG_CONFIG_PATH={prefix}/lib/pkgconfig:$PKG_CONFIG_PATH"
                ) )
    return scr

async def to_script(config : Config,
              prefix : str,
              data : Any,
              stype : str) -> cmd.Script:
    specs  = load_env(data)
    
    scr = prelude( config, prefix )
    for spec in specs:
        if stype == "install":
            scr.extend( spec.installScript() )
        else:
            scr.extend( spec.loadScript() )
    return scr

@app.command()
def load(specfile : Path = typer.Argument(..., help="Environment spec file."),
         v : V1 = False,
         vv : V2 = False,
         cfg : CfgArg = None):
    """
    Load a specfile (assuming components are installed).
    """
    setup_logging(v, vv)
    config = load_config(cfg)
    data = read_data(specfile)
    scr = arun( to_script(config, specfile.name, data, "load") )
    print(scr)

@app.command()
def install(specfile : Path = typer.Argument(..., help="Environment spec file."),
         v : V1 = False,
         vv : V2 = False,
         cfg : CfgArg = None):
    """
    Install a specfile.
    """
    setup_logging(v, vv)
    config = load_config(cfg)
    data = read_data(specfile)
    scr = arun( to_script(config, specfile.name, data, "install") )
    print(scr)

    #raise typer.Exit(code=err)
