def quote(x):
    return "'" + x.replace("'", "''") + "'"

class Cmd:
    def __init__(self, x, once):
        self.x = x
        self.once = once
    def __str__(self):
        return self.x

class Script:
    """ A script is modeled as a monoid (associative,
        with empty + addition rules),
        where append joins all commands together in-order.

        Commands marked "once" are only appended 
    """
    def __init__(self) -> None:
        self.cmds : list[Cmd] = []
        self.done : set[str] = set() # (invariant)= set(c.x for c in self.cmds)

    def _append(self, cmd : Cmd) -> None:
        "Internal, unguarded append of cmd."
        if len(cmd.x.strip()) == 0:
            return
        self.done.add( cmd.x )
        self.cmds.append( cmd )

    def run(self, cmd : Cmd) -> None:
        if cmd.once and cmd.x in self.done:
            return
        self._append(cmd)

    def extend(self, rhs : "Script") -> "Script":
        """ In-place update by extending self
            with the commands from the right-hand side.
            This is the monoid addition rule.
        """
        for c in rhs.cmds:
            if c.once and c.x in self.done:
                continue
            self._append(c)
        return self

    def __str__(self) -> str:
        return "\n".join( map(str, self.cmds) )

def run(*cmds : str) -> Script:
    S = Script()
    for cmd in cmds:
        S.run( Cmd(cmd, False) )
    return S

def runonce(*cmds : str) -> Script:
    S = Script()
    for cmd in cmds:
        S.run( Cmd(cmd, True) )
    return S
