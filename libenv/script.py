from .envtype import EnvType
from . import cmd

class Script(EnvType):
    install: str
    load: str

    def installScript(self) -> cmd.Script:
        return cmd.runonce( self.install )
    def loadScript(self) -> cmd.Script:
        return cmd.run( self.load )
