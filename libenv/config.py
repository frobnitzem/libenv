from typing import Union
import os
from pathlib import Path

from pydantic import BaseModel

class Config(BaseModel):
    data_dir    : Path
    concurrency : int

def load_config(path : Union[str, os.PathLike, None]) -> Config:
    cfg_name = "libenv.json"
    if path is None:
        path1 = Path(os.environ["HOME"]) / '.config' / cfg_name
    else:
        path1 = Path(path)
    assert path1.exists(), f"{cfg_name} is required to exist (tried {path1})"
    config = Config.model_validate_json(
                        path1.read_text(encoding='utf-8'))
    # additional validation steps...
    return config

