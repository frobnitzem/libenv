import pytest

from libenv.config import Config, load_config

def mkconfig(tmp_path):
    data   = tmp_path / "libenv_data"
    data.mkdir(exist_ok=True)
    config = Config(data_dir=data, concurrency=4)
    cfg    = tmp_path / "libenv.json"
    cfg.write_text(config.model_dump_json(), encoding="utf-8")
    return cfg, config

def test_config(tmp_path):
    data = tmp_path / "libenv_data"
    with pytest.raises(ValueError):
        config = Config(data_dir = data)

    cfg, config = mkconfig(tmp_path)
    config2 = load_config(cfg)

    assert config == config2
