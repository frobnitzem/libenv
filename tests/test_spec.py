import pytest
from pydantic import ValidationError

from .test_config import mkconfig

from libenv.spec import Spec, load_spec, read_data

def test_spec(tmp_path):
    cfg, config = mkconfig(tmp_path)
    
    specfile = tmp_path / "spec.json"
    spec = Spec(type = "test", a = 123, b = ["x", "y"])
    specfile.write_text(spec.model_dump_json(), encoding="utf-8")
    with pytest.raises(ModuleNotFoundError):
        spec2 = load_spec(read_data(specfile))

    spec.type = "Var"
    specfile.write_text(spec.model_dump_json(), encoding="utf-8")
    with pytest.raises(ValidationError):
        spec3 = load_spec(read_data(specfile))

    specfile.write_text('{"type": "Var", "vars": {"foo":"bar"}}\n',
                        encoding="utf-8")
    spec4 = load_spec(read_data(specfile))
    assert len(spec4.vars) == 1
    assert spec4.vars["foo"] == "bar"
