import pytest
from pydantic import ValidationError

from .test_config import mkconfig

from libenv import cmd
from libenv.spec import Spec, load_spec, read_data

def test_spec(tmp_path):
    cfg, config = mkconfig(tmp_path)
    
    specfile = tmp_path / "spec.yaml"
    specfile.write_text('type: Var\nvars: {"foo":"bar"}\n',
                        encoding="utf-8")
    spec = load_spec(read_data(specfile))
    assert len(spec.vars) == 1
    assert spec.vars["foo"] == "bar"
    load = spec.loadScript()
    assert isinstance(load, cmd.Script)
    assert str(load) == "foo = 'bar'"

    inst = spec.installScript()
    assert isinstance(inst, cmd.Script)
