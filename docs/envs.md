Libenv uses the following file path and environment variable
conventions.

If unset, each variable defaults to the value
in the parentheses.

* `LIBENV_CONFIG` (`$HOME/.config/libenv.json`)

  Path to the libenv configuration file. Can be set to
  any location. Must be an absolute path.

* `LIBENV_DATA_DIR` (value from `LIBENV_CONFIG`)

  The location where libenv will install plugins, shims
  and packages. Can be set to any location.
  Must be an absolute path.

* `LIBENV_CONCURRENCY` (value from `LIBENV_CONFIG`)

  Number of cores to use when compiling source code.
  If set, this value takes precedence over the config
  concurrency value.

