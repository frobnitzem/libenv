Python configures itself using just one variable,
`VIRTUAL_ENV`.  This names the root of a local
install tree (setup like `/usr`), containing,
- bin/
- include/
- lib/
- lib64/
- share/
- pyvenv.cfg

For convenience in executing programs from the
virtual environment, it is common practice
to add `$VIRTUAL_ENV/bin` to `PATH`.
