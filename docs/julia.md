The following variables must be set before Julia runs,

    JULIA_NUM_THREADS = max threads available to Julia (Julia attempts to detect if not set)
    JULIA_BINDIR      = path to Julia executable
    JULIA_PROJECT     = path to current "--project" directory
    JULIA_DEPOT_PATH  = path to package index

A [number of other configuration options](https://docs.julialang.org/en/v1/manual/environment-variables/)
can be set programmatically using a configuration file,

    ~/.julia/config/startup.jl
