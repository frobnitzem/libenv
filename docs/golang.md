Golang uses two environment variables
to configure its behavior:

- `GOROOT` points to the installation directory for the golang SDK
- `GOPATH` points to a directory containing go source and installed packages
  - `src/`
  - `pkg/`
  - `bin/`
